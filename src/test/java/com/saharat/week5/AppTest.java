package com.saharat.week5;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void shouldAdd1AndIS2(){
        int result =App.add(1, 1);
        assertEquals(2, result);
        
    }
    @Test
    public void shouldAdd1AndIS3(){
        int result =App.add(2, 1);
        assertEquals(3, result);
    }
}

